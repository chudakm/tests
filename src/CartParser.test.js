import CartParser from './CartParser.js';
import cartJson from "../samples/cart.json"
import fs from "fs"

let parser;

beforeEach(() => {
	parser = new CartParser();
});

afterEach(() => {
	parser = new CartParser();
})

describe('CartParser - unit tests', () => {
	it('should throw an error if validation failed', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,-1`;
		parser.readFile = jest.fn().mockImplementation(() => content);

		const actual = () => parser.parse(content);

		expect(() => actual()).toThrow()
	});

	it('should successfully parse', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;
		parser.readFile = jest.fn().mockImplementation(() => content);

		const actual = parser.parse(content);

		expect(actual).toHaveProperty('items');
		expect(actual).toHaveProperty('total');
	});

	it('should call readFileSync', () => {
		const path = "/samples/cart.csv";
		const spy = jest.spyOn(fs, 'readFileSync').mockImplementation(() => '');

 
		parser.readFile(path);

		
		expect(spy).toHaveBeenCalled();
		spy.mockRestore();
	});

	it('should throw an error if csv header names are incorrect', () => {
		const content = `Prodyct name,Praice,Kvontity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const actual = parser.validate(content);
		
		expect(actual).toHaveLength(3)
		expect(actual[0].message).toBe('Expected header to be named "Product name" but received Prodyct name.');
		expect(actual[1].message).toBe('Expected header to be named "Price" but received Praice.');
		expect(actual[2].message).toBe('Expected header to be named "Quantity" but received Kvontity.');
	})

	it('should throw an error if csv row cell length is lower than needed', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90`;

		const actual = parser.validate(content);


		expect(actual).toHaveLength(1);
		expect(actual[0].message).toBe(`Expected row to have 3 cells but received 2.`);
	})

	it('should throw an error if cell string value is empty', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		,18.90,1`;

		const actual = parser.validate(content);


		expect(actual).toHaveLength(1);
		expect(actual[0].message).toBe(`Expected cell to be a nonempty string but received "".`);
	})
	
	
	it('should throw an error if cell number value is lower than 0', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Tvoluptatem,18.90,-1`;

		const actual = parser.validate(content);


		expect(actual).toHaveLength(1);
		expect(actual[0].message).toBe(`Expected cell to be a positive number but received "-1".`);
	})

	it('should successfully validate valid csv content ', () => {
		const content = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const actual = parser.validate(content);
		expect(actual).toHaveLength(0);

	})

	it('should correctly convert csv file line to a json object and add id property', () => {
		const sampleLine = 'Mollis consequat,9.00,2';
		const uuidV4RegExp = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i

		const actual = parser.parseLine(sampleLine);

		expect(actual).toHaveProperty('name', 'Mollis consequat');
		expect(actual).toHaveProperty('price', 9);
		expect(actual).toHaveProperty('quantity', 2);
		expect(actual).toHaveProperty('id');
		expect(actual.id).toMatch(uuidV4RegExp);
	});

	it('should correctly calculate total price of items', () => {
		const actual = parser.calcTotal(cartJson.items);
		const expected = cartJson.total;

		expect(actual).toBeCloseTo(expected);
	})

	it('should create error', () => {
		const type = Error;
		const row = '1'
		const column = '1'
		const message = 'message'

		const actual = parser.createError(type, row, column, message);

		expect(actual).toHaveProperty('type');
		expect(actual).toHaveProperty('row');
		expect(actual).toHaveProperty('column');
		expect(actual).toHaveProperty('message');
		expect(actual.type).toBe(type);
		expect(actual.row).toBe(row);
		expect(actual.column).toBe(column);
		expect(actual.message).toBe(message);
	});

});

describe('CartParser - integration test', () => {
	it('should parse csv file correctly', () => {
		const csvPath = 'samples/cart.csv';
		const uuidV4RegExp = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i

		const actual = parser.parse(csvPath);

		expect(actual).toHaveProperty('items');
		expect(actual).toHaveProperty('total');
		expect(actual.items).toHaveLength(5);
		for (let i = 0; i < actual.items.length; i++) {
			const item = actual.items[i];
			expect(item).toHaveProperty('name', cartJson.items[i].name);
			expect(item).toHaveProperty('price', cartJson.items[i].price);
			expect(item).toHaveProperty('quantity', cartJson.items[i].quantity);
			expect(item).toHaveProperty('id');
			expect(item.id).toMatch(uuidV4RegExp);
		}
	});
});